# pipelinedevops

### On site
## Run docker file
# docker build -t warfile .
# docker run -tdi –-name tomtest -p 8080:8080 warfile

### On GCP
# CD in the Dockerfile location
# 0 - Push in docker registry: gcloud builds submit --tag gcr.io/$GOOGLE_CLOUD_PROJECT/hellojarfile
# 0b - Test locally: docker run -d -p 8080:8080 gcr.io/$GOOGLE_CLOUD_PROJECT/hellojarfile
# 1 - Deploy in cloud run : gcloud run deploy --image gcr.io/$GOOGLE_CLOUD_PROJECT/hellojarfile --allow-unauthenticated --region=$LOCATION
# 2 - delete image: gcloud container images delete gcr.io/$GOOGLE_CLOUD_PROJECT/hellojarfile
# 3 - delete service: gcloud beta run services delete hellojarfile

## CI/CD conf jenkinsfile: 
 # test without commit project_token (bad pratice)
