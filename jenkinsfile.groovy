def projrct_token = 'glpat-Y6S45MkQrkjfJoZiDmKs'
properties([
        gitLabConnection('si_connection'),
        pipelineTriggers([
                [
                        $class: 'GitLabPushTrigger',
                        branchFilter: 'All',
                        triggerOnPush: true,
                        triggerOpenMergeRequestOnpush: "never",
                        triggerOnNoteRequest: true,
                        noteRegex: "Jenkins please retry a build",
                        skipWorkInProgressMergeRequest: true,
                        secretToken: projrct_token,
                        ciSkip: false,
                        setBuildDescription: true,
                        addNoteOnMergeRequest: true,
                        addCiMessage: true,
                        addVoteOnMergeRequest: true,
                        acceptMergeRequestOnSuccess: true,
                        branchFilterType: "NameBasedFilter",
                        includeBranchesSpec: "",
                        excludeBranchesSpec: "",
                ]
        ])
])
node{
    cleanWs()
    try{
        stage('Stape 1'){
            sh "echo 'hello world !!'"
        }
        stage('Stape 2'){
            sh "echo 'hello world2 !!'"
        }
    }finally{
        cleanWs()
    }
}
