FROM alpine/git as clone
WORKDIR /app
RUN git clone https://gitlab.com/spring-microservices4/pipelinedevops.git

FROM matderosa/maven-3.8-openjdk-17-gcc8.5:latest as build
WORKDIR /app
COPY --from=clone /app/pipelinedevops /app
RUN mvn package

FROM tomcat:jre8
COPY --from=build /app/target/pipelinedevops.jar pipelinedevops.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","pipelinedevops.jar"]