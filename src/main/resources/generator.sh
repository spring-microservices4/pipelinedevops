#!/bin/bash

###########################################
##
## Generator docker compose
##
###########################################
## Variables ###########################################
DIR="${HOME}/generator"
USER_SCRIPT=${USER}
## Functions ###########################################
help(){
  echo "USAGE :
    ${0##*/} [-h] [--help]
    Options :
      -h, --help : aides
      -p, --postgres: launch a server or an instance
	  -i, --ip: affichage des ip
  "
}

ip(){
	for i in $(docker ps -q); do docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} - {{.Name}}" $i;done
}
parser_options(){
  case $@ in
          -h|--help)
            help
            ;;
          -p|--postgres)
            postgres
            ;;
            *)
            echo "invalid option, lancer -h --help"
  esac
}

postgres(){
  echo ""
  echo "Installation de postgres...."
  echo ""
  echo "1 - Création du repertoir des datas.."
  mkdir -p $DIR/postgres
  echo ""
  
echo "
services:
  postgres:
    container_name: postgres
    image: postgres
    environment:
      POSTGRES_USER: marius
      POSTGRES_PASSWORD: password
      PGDATA: /data/postgres
    volumes:
      - postgres:/data/postgres
    ports:
      - "5432:5432"
    networks:
      - postgres
    restart: unless-stopped
  pgadmin:
    container_name: pgadmin
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: ${PGADMIN_DEFAULT_EMAIL:-pgadmin4@pgadmin.org}
      PGADMIN_DEFAULT_PASSWORD: ${PGADMIN_DEFAULT_PASSWORD:-admin}
      PGADMIN_CONFIG_SERVER_MODE: 'False'
    volumes:
      - pgadmin:/var/lib/pgadmin
    ports:
      - "5050:80"
    networks:
      - postgres
    restart: unless-stopped
networks:
  postgres:
    driver: bridge
volumes:
  postgres:
  pgadmin:
" > docker-compose-pg.yml
docker compose -f docker-compose-pg.yml up -d
	
	echo ""
	echo "
	Credentials: 
	PORT: 5432
		POSTGRES_USER: marius
	    POSTGRES_PASSWORD: password
		
	Command: psql -h <ip> -u marius -d <db>
	"
}

## Execute ###########################################
parser_options $@
ip