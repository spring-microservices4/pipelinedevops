package com.dmapp.post.user.Services;

public record CustomerRegistrationRequest(String firstName, String lastName, String email){}
